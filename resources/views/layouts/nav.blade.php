<nav class="flex items-center justify-between flex-wrap bg-teal-900  p-0 font-mono">
    <div class="flex items-center flex-shrink-0 text-white mr-6 ml-2">
        <img src="/images/logo.png" class="fill-current h-9 w-9 mr-2 " width="54" height="54" viewBox="0 0 54 54"/>
        <span class="font-semibold text-xl tracking-tight">Cinema</span>
    </div>
    <div class="w-full block flex-grow flex items-center w-auto text-base">
            <router-link to="/" exact class="block mt-0 inline-block mt-0 text-white  mr-4 mt-4">
                <a href="#responsive-header" class="font-mono">
                    Home
                </a>
            </router-link>

            @if(Auth::user()->type =="seller")
                <router-link to="/Tickets" exact class="block mt-0 inline-block mt-0 text-white  mr-4 mt-4">
                    <a href="#responsive-header" class="font-mono">
                        Add Ticket
                    </a>
                </router-link>
            @endif
            <div class=" absolute right-0 ">
                <div class="buttons">
                    <a class="button is-primary " href="{{ route('logout') }}"
                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                        {{ __('Logout') }}
                    </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </div>
            </div>
        </div>
</nav>
