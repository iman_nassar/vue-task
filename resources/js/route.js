import VueRouter from "vue-router";

let routes = [{
    path: '/',
    name: 'home',
    component: require("../components/home").default
},
    {
        path: '/Tickets',
        name: 'add-ticket',
        component: require("../components/ticket/add-ticket").default
    },
    {
        path: '/edit-ticket',
        name: 'edit-ticket',
        component: require("../components/ticket/edit-ticket").default,
        props: true
    }, {
        path: '/purchase-ticket',
        name: 'purchase-ticket',
        component: require("../components/ticket/purchase-ticket").default,
        props: true
    }
]
export default new VueRouter({
    routes,
    linkActiveClass: "is-active"
})
