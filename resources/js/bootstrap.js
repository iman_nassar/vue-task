import Vue from 'vue';
import VueRouter from 'vue-router'
import Form from './form';
import axios from 'axios';


window.Vue=Vue;
Vue.use(VueRouter);

window.Form=Form;
window._ = require('lodash');


try {
    window.Popper = require('popper.js').default;
    window.$ = window.jQuery = require('jquery');

    require('bootstrap');
} catch (e) {}



window.axios=axios;
window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
