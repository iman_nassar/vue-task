<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::resource("ticket", "TicketController");
Route::get('/ticket/purchase/{ticketId}', 'TicketController@getPurchase');
Route::post('/ticket/purchase/{ticketId}', 'TicketController@purchase');
Route::get('/Auth', function (){
    $user=['id'=>Auth::user()->id,'type'=>Auth::user()->type];
    return $user;
});


