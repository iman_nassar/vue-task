<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    protected $guarded = [];
    public $timestamps = false;

    public function calculateTotalPrice($quantity)
    {
        return $this->price * $quantity;
    }

    public function calculateRemainingTickets($quantity)
    {
        return $this->quantity - $quantity;
    }
}
