<?php

namespace App\Http\Controllers;

use App\Ticket;
use App\TicketPurchase;
use App\User;
use App\Order;
use http\Env\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\TicketRequest;
use App\Http\Requests\PurchaseRequest;
use App\Mail\Mail;

class TicketController extends Controller
{
    public function __construct()
    {
    }

    public function index()
    {
        if (Auth::user()->type == 'seller') {
            $tickets = Ticket::where([['seller_id', Auth::user()->id]])->get();
        } else {
            $tickets = Ticket::where([['status', 1], ['quantity', '>', 0]])->get();
        }
        return ["tickets" => $tickets];
    }

    public function store(TicketRequest $request)
    {
        $attributes = $request->validated();
        $attributes["status"] = $attributes["status"] ?? 0;
        $attributes["seller_id"] = Auth::user()->id;

        Ticket::create($attributes);
        return ["message" => "Successfully Added Ticket"];
    }


    public function update(TicketRequest $request, Ticket $ticket)
    {

        $attributes = $request->validated();
        $attributes["status"] = $attributes["status"] ?? 0;
        $attributes["seller_id"] = Auth::user()->id;
        $ticket->update($attributes);
        return ["message" => "Successfully Updated Ticket"];


    }

    public function destroy(Ticket $ticket)
    {
        $ticket->delete();
        return ["message" => "Successfully Deleted"];
    }


    public function purchase(PurchaseRequest $request, $id)
    {

        $orderAttributes = $request->validated();

        $orderAttributes["ticket_id"] = $id;
        $orderAttributes["user_id"] = Auth::user()->id;


        $user = User::find($orderAttributes['user_id']);
        $ticket = Ticket::findOrFail($id);
        if ($ticket->quantity < $orderAttributes['quantity'])
            return response('Quantity out of bound', 409);

        $totalPrice = $ticket->calculateTotalPrice($orderAttributes['quantity']);

        $orderAttributes["totalPrice"] = $totalPrice;

        if ($user['balance'] >= $totalPrice) {

            $order = new Order();
            $orderId = $order::create($orderAttributes)->id;
            TicketPurchase::insertPurchase($orderId, $orderAttributes['quantity']);


            $UserUpdateAttr = ['balance' => $user->calculateRemainingBalance($totalPrice)];
            $TicketUpdateAttr = ['quantity' => $ticket->calculateRemainingTickets($orderAttributes['quantity'])];

            $ticket->update($TicketUpdateAttr);
            $user->update($UserUpdateAttr);


            return ["message" => "Successfully Purchased"];

        } else {
            return ["message" => "Purchase Failure : Insufficient fund"];
        }
    }


}
