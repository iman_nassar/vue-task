<?php

namespace App;

use App\Mail\Mail;
use Hashids\Hashids;
use Illuminate\Database\Eloquent\Model;


class TicketPurchase extends Model
{
    protected $guarded = [];
    public $timestamps = false;

    public static function insertPurchase($orderId, $quantity)
    {
        $lastInsertedId=TicketPurchase::getLatestPurchaseId();

        while ($quantity > 0) {
            $hashKey = config('app.hash_key', 'Laravel');
            $hashids = new Hashids($hashKey, 36);
            $code = $hashids->encode($lastInsertedId);
            $attribute = ['order_id' => $orderId, 'code' => $code];
            TicketPurchase::query()->insert($attribute);
            --$quantity;
            ++$lastInsertedId;
        }


    }
public static function getLatestPurchaseId(){
    $lastPurchase = TicketPurchase::all()->last();
    $lastInsertedId = 0;

    if ($lastPurchase != null)
        $lastInsertedId = $lastPurchase->id;

    return $lastInsertedId;
//    return TicketPurchase::query()->latest()->id;

}

}
